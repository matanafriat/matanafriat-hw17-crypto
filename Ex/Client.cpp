#include "Client.h"
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>
#include <hex.h>

CryptoDevice cryptoDevice;

//MD5
byte digest[CryptoPP::Weak::MD5::DIGESTSIZE];
string username = "jamesbond", password = "9E94B15ED312FA42232FD87A55DB0D39";

bool login();

int process_client(client_type &new_client)
{
	while (1)
	{
		memset(new_client.received_message, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{


			int iResult = recv(new_client.socket, new_client.received_message, DEFAULT_BUFLEN, 0);

			string strMessage(new_client.received_message);

			// some logic, we dont want to decrypt notifications sent by the operator
			// our protocol says - ": " means notification from the operator
			size_t position = strMessage.find(": ") + 2;
			string prefix = strMessage.substr(0, position);
			string postfix = strMessage.substr(position);
			string decrypted_message;

			//this is the only notification we use right now :(
			if (postfix != "Disconnected")
			{
				//please decrypt this part!
				postfix = cryptoDevice.decryptAES(postfix); //decryption
				decrypted_message = postfix;
			}
			else
				//dont decrypt this - not classified and has not been ecrypted! 
				//trying to do so may cause errors
				decrypted_message = postfix;

			if (iResult != SOCKET_ERROR)
				cout << prefix + postfix << endl;
			else
			{
				cout << "recv() failed: " << WSAGetLastError() << endl;
				break;
			}
		}
	}

	if (WSAGetLastError() == WSAECONNRESET)
		cout << "The server has disconnected" << endl;

	return 0;
}

int main()
{
	WSAData wsa_data;
	struct addrinfo *result = NULL, *ptr = NULL, hints;
	string sent_message = "";
	client_type client = { INVALID_SOCKET, -1, "" };
	int iResult = 0;
	string message;

	cout << "Starting Client...\n";

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (iResult != 0) {
		cout << "WSAStartup() failed with error: " << iResult << endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	cout << "Connecting...\n";
	if (!login())
	{
		return 1; //login failed
	}

	// Resolve the server address and port
	iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		cout << "getaddrinfo() failed with error: " << iResult << endl;
		WSACleanup();
		system("pause");
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		client.socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (client.socket == INVALID_SOCKET) {
			cout << "socket() failed with error: " << WSAGetLastError() << endl;
			WSACleanup();
			system("pause");
			return 1;
		}

		// Connect to server.
		iResult = connect(client.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(client.socket);
			client.socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (client.socket == INVALID_SOCKET) {
		cout << "Unable to connect to server!" << endl;
		WSACleanup();
		system("pause");
		return 1;
	}

	cout << "Successfully Connected" << endl;

	//Obtain id from server for this client;
	recv(client.socket, client.received_message, DEFAULT_BUFLEN, 0);
	message = client.received_message;

	if (message != "Server is full")
	{
		client.id = atoi(client.received_message);

		thread my_thread(process_client, client);

		while (1)
		{
			getline(cin, sent_message);

			//top secret! please encrypt
			string cipher = cryptoDevice.encryptAES(sent_message); //encryption

			iResult = send(client.socket, cipher.c_str(), cipher.length(), 0);

			if (iResult <= 0)
			{
				cout << "send() failed: " << WSAGetLastError() << endl;
				break;
			}


		}

		//Shutdown the connection since no more data will be sent
		my_thread.detach();
	}
	else
		cout << client.received_message << endl;

	cout << "Shutting down socket..." << endl;
	iResult = shutdown(client.socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		cout << "shutdown() failed with error: " << WSAGetLastError() << endl;
		closesocket(client.socket);
		WSACleanup();
		system("pause");
		return 1;
	}

	closesocket(client.socket);
	WSACleanup();
	system("pause");
	return 0;
}

//checks username and password, if incorrect 3 times in a row, returns false
bool login()
{
	string namepass, name, pass;
	size_t pos;
	CryptoPP::Weak::MD5 hash;
	cout << "register secretly! (username space password):" << endl;

	for (int i = 1; i <= 3; i++)
	{
		getline(cin, namepass);
		pos = namepass.find(" ");
		name = namepass.substr(0, pos);
		pass = namepass.substr(pos + 1);

		//MD5 conversion
		hash.CalculateDigest(digest, (const byte*)pass.c_str(), pass.length());

		CryptoPP::HexEncoder encoder;
		std::string encodedpass;

		encoder.Attach(new CryptoPP::StringSink(encodedpass));
		encoder.Put(digest, sizeof(digest));
		encoder.MessageEnd();

		if (encodedpass.compare(password) == 0 && name.compare(username) == 0)
		{
			cout << "name: " << username << endl << "password: " << password << endl << "Agent accepted! welcome in!" << endl;
			return true;
		}
		else
		{
			if (i == 3)
			{
				cout << "Wrong username or password - no more tries! exiting device..." << endl;
			}
			else
			{
				cout << "Wrong username or password - try again (" << 3 - i << " more tries)" << endl;
			}
		}
	}
	return false;
}